from typing import Union
import json
from fastapi import FastAPI
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
from sklearn.metrics.pairwise import cosine_similarity
import tensorflow_hub as hub
import numpy as np
import pandas as pd



def load_USE_encoder(module):
    with tf.Graph().as_default():
        sentences = tf.placeholder(tf.string)
        embed = hub.Module(module)
        embeddings = embed(sentences)
        session = tf.train.MonitoredSession()
    return lambda x: session.run(embeddings, {sentences: x})


encoder = load_USE_encoder('./USEE')

# module_url = "https://tfhub.dev/google/universal-sentence-encoder/4"
# model = hub.load(module_url)

app = FastAPI()



@app.get("/title/{title_id}")
def read_root(title_id: str):
    df = pd.read_csv('ipc_data.csv')
    df.head()
    message_embeddings = encoder(df['Offence'])
    query_vec = encoder([title_id])
    similarity = cosine_similarity(query_vec, message_embeddings).flatten()
    indices = np.argpartition(similarity, -3)[-3:]
    results = df.iloc[indices].iloc[::-1].to_json(orient="records")
    parsed = json.loads(results)
    return parsed


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}



# To Run
# uvicorn main:app --reload

# Eg: http://127.0.0.1:8000/title/murder